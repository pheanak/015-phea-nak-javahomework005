
public class Main {
    public static void main(String[] args) {
        String message = "Hello KSHRD! \n************************************* " +
                "\nI will try my best to be here at HRD. \n------------------------------------- " +
                "\nDownloading...........";
        slowPrint(message, 250);
    }

    public static void slowPrint(String message, long millsPerChar) {
        for (int i = 0;i<message.length();i++){
            System.out.print(message.charAt(i));
            try
            {
                Thread.sleep(millsPerChar);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
        System.out.print("Completed 100%!");
    }
}
